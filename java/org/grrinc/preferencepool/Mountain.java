package org.grrinc.preferencepool;

import android.util.Log;

/**
 * Created by guguza on 18.06.2015.
 */
public class Mountain extends Score {
    public static final String TAG = "MOUNTAIN";

    public Mountain(int id, int quantity) {
        super(id, quantity);
    }

    public Mountain(int id) {
        super(id);
    }

    @Override
    public String print() {
        String info = "ID: " + getId() + ", quantity = " + getQuantity();
        Log.d(TAG, info);
        return info;
    }
}
