package org.grrinc.preferencepool.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;

import org.grrinc.preferencepool.R;
import org.grrinc.preferencepool.fragment.RulesFragment;
import org.grrinc.preferencepool.fragment.SettingsFragment;
import org.grrinc.preferencepool.util.CustomFragmentManager;


public class WupsenActivity extends Activity {

    CustomFragmentManager customFragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_another_main);

        CustomFragmentManager.getInstance().init(this);
        customFragmentManager = CustomFragmentManager.getInstance();

        Intent intent = getIntent();
        int menuId = intent.getIntExtra("menu_id", 0);

        Log.d("id", menuId + "");

        switch (menuId) {
            case R.id.action_settings: {
                SettingsFragment settingsFragment = new SettingsFragment();
                customFragmentManager.setFragment(R.id.container, settingsFragment, false);
                break;
            }
            case R.id.action_rules: {
                RulesFragment rulesFragment = new RulesFragment();
                customFragmentManager.setFragment(R.id.container, rulesFragment, false);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_another_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }


}
