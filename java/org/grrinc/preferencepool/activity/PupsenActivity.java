package org.grrinc.preferencepool.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;

import org.grrinc.preferencepool.GameGeometry;
import org.grrinc.preferencepool.R;
import org.grrinc.preferencepool.util.CustomFragmentManager;

/**
 * Created by tuule on 18.06.15.    
 */
public class PupsenActivity extends AppCompatActivity {
    private ActionBar mActionBar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        mActionBar = getSupportActionBar();
        assert mActionBar != null;
        mActionBar.hide();

        setContentView(R.layout.pool_layout);

        CustomFragmentManager.getInstance().init(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
       int id = item.getItemId();

        Intent intent = new Intent(this, WupsenActivity.class);

        intent.putExtra("menu_id", id);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        mActionBar.hide();

        startActivity(intent);

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
