package org.grrinc.preferencepool.fragment;

import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceCategory;
import android.preference.PreferenceFragment;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.animation.Animation;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toolbar;

import org.grrinc.preferencepool.R;
import org.grrinc.preferencepool.util.Dialogs;

/**
 * Created by wetalkas on 19.06.15.
 */
public class SettingsFragment extends PreferenceFragment{



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.xml.preferences);





        ListPreference lpNumberOfPlayers = (ListPreference)getPreferenceManager()
                .findPreference("number_of_players");

        lpNumberOfPlayers.setSummary(lpNumberOfPlayers.getValue());

        lpNumberOfPlayers.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                preference.setSummary(newValue.toString());
                return true;
            }
        });

        EditTextPreference etpWhistPrice = (EditTextPreference) getPreferenceManager()
                .findPreference("whist_price");

        String whistPrice = etpWhistPrice.getText();
        if (whistPrice.length() > 0) {
            etpWhistPrice.setSummary(whistPrice);
        }

        etpWhistPrice.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                EditText et = ((EditTextPreference) preference).getEditText();
                et.setText(preference.getSummary());
                int index = et.length();
                ((EditTextPreference) preference).getEditText().setSelection(index);
                return true;
            }
        });


        etpWhistPrice.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                if (newValue.toString().length() > 0) {
                    preference.setSummary(newValue.toString());
                } else {
                    preference.setSummary("1");
                }
                return true;
            }
        });


        EditTextPreference etpCurrencyType = (EditTextPreference) getPreferenceManager()
                .findPreference("currency_type");

        String currencyType = etpCurrencyType.getText();
        if (currencyType.length() > 0) {
            etpCurrencyType.setSummary(etpCurrencyType.getText());
        }

        etpCurrencyType.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                EditText et = ((EditTextPreference)preference).getEditText();
                et.setText(preference.getSummary());
                int index = et.length();
                ((EditTextPreference)preference).getEditText().setSelection(index);
                return true;
            }
        });

        etpCurrencyType.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                if (newValue.toString().length() > 0) {
                    preference.setSummary(newValue.toString());
                } else {
                    preference.setSummary("UAH");
                }
                return true;
            }
        });



        setHasOptionsMenu(true);

    }





    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_settings_fragment, menu);
    }


    @Override
    public void onResume() {
        super.onResume();

        View rootView = getView();
        if (rootView != null) {
            ListView list = (ListView) rootView.findViewById(android.R.id.list);
            //list.setPadding(0, 0, 0, 0);
            list.setDivider(null);
        }
    }
}
