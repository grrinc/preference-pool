package org.grrinc.preferencepool.fragment;

import android.os.Bundle;
import android.app.Fragment;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.SimpleExpandableListAdapter;

import org.grrinc.preferencepool.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class RulesFragment extends Fragment {

    String[] conventions = new String[]{"Сочи", "Ленинград(Питер)", "Ростов", "Классика", "Скачки", "Типы вистов"};
    String[] convOptsSochi;
    String[] convOptsPeter;
    String[] convOptsRostov;
    String[] convOptsClassic;
    String[] convOptsRaces;
    String[] convOptsWhists;

    ArrayList<ArrayList<Map<String, String>>> convDataOpts;

    Map<String, String> m;

    ExpandableListView elvMain;

    View rootView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().getApplicationContext();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_rules, container, false);

        getActivity().getApplicationContext();

        if (isAdded()) {
            convOptsSochi = new String[]{getResources().getString(R.string.sochi)};
            convOptsPeter = new String[]{getResources().getString(R.string.peter)};
            convOptsRostov = new String[]{getResources().getString(R.string.rostov)};
            convOptsClassic = new String[]{getResources().getString(R.string.classic)};
            convOptsRaces = new String[]{getResources().getString(R.string.races)};
            convOptsWhists = new String[]{getResources().getString(R.string.whist1),
                    getResources().getString(R.string.whist2),
                    getResources().getString(R.string.whist3),
                    getResources().getString(R.string.whist4)};
        }

        ArrayList<Map<String, String>> convData = new ArrayList<>();
        for (String convention : conventions) {
            m = new HashMap<>();
            m.put("convName", convention);
            convData.add(m);
        }

        String convFrom[] = new String[]{"convName"};
        int convTo[] = new int[]{android.R.id.text1};

        convDataOpts = new ArrayList<>();

        setElement(convOptsSochi);
        setElement(convOptsPeter);
        setElement(convOptsRostov);
        setElement(convOptsClassic);
        setElement(convOptsRaces);
        setElement(convOptsWhists);

       // String convOptsFrom[] = new String[]{"convName"};
       // int convOptsTo[] = new int[]{android.R.id.text1};

        SimpleExpandableListAdapter adapter = new SimpleExpandableListAdapter(
                getActivity().getBaseContext(),
                convData,
                R.layout.expandable_title,
                convFrom,
                convTo,
                convDataOpts,
                R.layout.expandable_textview,
                convFrom,
                convTo
        );

        elvMain = (ExpandableListView) rootView.findViewById(R.id.elvMain);
        elvMain.setAdapter(adapter);

        return rootView;
    }

    public int GetDipsFromPixel(float pixels)
    {
        // Get the screen's density scale
        final float scale = getResources().getDisplayMetrics().density;
        // Convert the dps to pixels, based on density scale
        return (int) (pixels * scale + 0.5f);
    }

    private void setElement(String[] convOptArray){
        ArrayList<Map<String, String>> convDataOptsItem = new ArrayList<>();

        for (String convOpt : convOptArray) {
            m = new HashMap<>();
            m.put("convName", convOpt);
            convDataOptsItem.add(m);
        }
        convDataOpts.add(convDataOptsItem);
    }

}


