package org.grrinc.preferencepool;

/**
 * Created by guguza on 18.06.2015.
 */
public abstract class Score implements Calculable{
    private int id;
    private int quantity;

    public Score(final int id, int quantity) {
        setId(id);
        setQuantity(quantity);
    }

    public Score(final int id) {
        setId(id);
    }

    @Override
    public int getQuantity() {
        return quantity;
    }

    @Override
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public void addQuantity(int number) {
        quantity += number;
    }

    @Override
    public void subtractQuantity(int number) {
        quantity -= number;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public abstract String print();
}
