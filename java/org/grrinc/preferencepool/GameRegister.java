package org.grrinc.preferencepool;

import android.util.Log;

import org.grrinc.preferencepool.logic.CustomGame;
import org.grrinc.preferencepool.logic.Game;
import org.grrinc.preferencepool.logic.Game.GameType;
import org.grrinc.preferencepool.logic.PeterGame;
import org.grrinc.preferencepool.logic.RostovGame;
import org.grrinc.preferencepool.logic.SochiGame;

/**
 * Created by guguza on 20.06.2015.
 */
public class GameRegister {
    private static volatile GameRegister instance;

    private Game game;
    private Player[] players;

    public static GameRegister getInstance() {
        GameRegister localInstance = instance;
        if(localInstance == null) {
            synchronized (GameRegister.class) {
                localInstance = instance;
                if(localInstance == null) {
                    instance = localInstance = new GameRegister();
                }
            }
        }
        return localInstance;
    }

    public void init(int numberOfPlayers, String[] nameArr) {
        players = new Player[numberOfPlayers];
        for(int i = 0; i < players.length; i ++) {
            players[i] = new Player(i, nameArr[i], numberOfPlayers);
        }
    }

    public Player getPlayer(int id) {
        return players[id];
    }

    public Player[] getPlayers() {
        return players;
    }

    public void onGameSet(GameType gameType, int numberOfPlayers, String[] nameArr) {
        init(numberOfPlayers, nameArr);
        switch(gameType) {
            //works
            case SOCHI:
                game = new Game(new SochiGame());
                break;
            case PETER:
                game = new Game(new PeterGame());
                break;
            case ROSTOV:
                game = new Game(new RostovGame());
                break;
            case CUSTOM:
                game = new Game(new CustomGame());
                break;
            default:
                Log.e("GameType", "Incorrect GameType");

        }
    }

    public boolean onDealConfirmed(DealType dealType, int playerId, int numberOfWhisters) {
        return game.executeStrategy(dealType, players, playerId, numberOfWhisters);
    }
}
