package org.grrinc.preferencepool;

import android.util.Log;

/**
 * Created by guguza on 18.06.2015.
 */
public class Pool extends Score {
    public static final String TAG = "POOL";

    public Pool(int id, int quantity) {
        super(id, quantity);
    }

    public Pool(int id) {
        super(id);
    }

    @Override
    public String print() {
        String info = "ID: " + getId() + ", quantity = " + getQuantity();
        Log.d(TAG, info);
        return info;
    }
}
