package org.grrinc.preferencepool;

/**
 * Created by guguza on 18.06.2015.
 */
public enum DealType {

    SIX, SEVEN, EIGHT, ALLPASS, NINE, TEN, MISERE;

    private int multiplierPool;
    private int multiplierMountain;

    public int getMultiplierPool() {
        return multiplierPool;
    }

    public void setMultiplierPool(int multiplierPool) {
        this.multiplierPool = multiplierPool;
    }

    public int getMultiplierMountain() {
        return multiplierMountain;
    }

    public void setMultiplierMountain(int multiplierMountain) {
        this.multiplierMountain = multiplierMountain;
    }
}