package org.grrinc.preferencepool;

/**
 * Created by guguza on 18.06.2015.
 */
public interface Calculable {
    int getQuantity();
    void setQuantity(int quantity);
    void addQuantity(int number);
    void subtractQuantity(int number);
}
