package org.grrinc.preferencepool.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.preference.Preference;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.Toast;

import org.grrinc.preferencepool.DealType;
import org.grrinc.preferencepool.GameRegister;
import org.grrinc.preferencepool.Player;
import org.grrinc.preferencepool.R;
import org.grrinc.preferencepool.util.adapter.DialogFourthPlayersTypesAdapter;

/**
 * Created by wetalkas on 27.06.15.
 */
public class Dialogs {

    private Activity inflateFrom; //no matter from? right? (context is below)
    private GameRegister gameRegister;
    private int dealTypeNumericValue;

    public Dialogs(Activity inflateFrom) {
        this.inflateFrom = inflateFrom;
    }

    public void showTapDialogFourPlayers(final Context context) {    // there
        final AlertDialog.Builder alert = new AlertDialog.Builder(context);

        ScrollView container = (ScrollView) inflateFrom.getLayoutInflater().inflate(R.layout.dialog_fourth_field_tap, null);

        final TextInputLayout tilDialogFirstPlayerScore = (TextInputLayout) container.findViewById(R.id.tilDialogFirstPlayerScore);
        final EditText etDialogFirstPlayerScore = tilDialogFirstPlayerScore.getEditText();

        final TextInputLayout tilDialogSecondPlayerScore = (TextInputLayout) container.findViewById(R.id.tilDialogsSecondPlayerScore);
        final EditText etDialogSecondPlayerScore = tilDialogSecondPlayerScore.getEditText();

        final TextInputLayout tilDialogThirdPlayerScore = (TextInputLayout) container.findViewById(R.id.tilDialogsThirdPlayerScore);
        final EditText etDialogThirdPlayerScore = tilDialogThirdPlayerScore.getEditText();

        final TextInputLayout tilDialogFourthPlayerScore = (TextInputLayout) container.findViewById(R.id.tilDialogsFourthPlayerScore);
        final EditText etDialogFourthPlayerScore = tilDialogFourthPlayerScore.getEditText();

        final TextInputLayout tilDialogTitle = (TextInputLayout) container.findViewById(R.id.tilDialogTitle);

        tilDialogTitle.setError("   Сумма взяток должна быть равна 10");
        final boolean[] titleErrorEnabled = {true};

        gameRegister = GameRegister.getInstance();

        tilDialogFirstPlayerScore.setHint(gameRegister.getPlayer(0).getName());
        tilDialogSecondPlayerScore.setHint(gameRegister.getPlayer(1).getName());
        tilDialogThirdPlayerScore.setHint(gameRegister.getPlayer(2).getName());
        tilDialogFourthPlayerScore.setHint(gameRegister.getPlayer(3).getName());

        final Spinner spDialogGameType = (Spinner) container.findViewById(R.id.spDialogGameType);
        final Spinner spDialogFirstPlayerType = (Spinner) container.findViewById(R.id.spDialogFirstPlayerType);
        final Spinner spDialogSecondPlayerType = (Spinner) container.findViewById(R.id.spDialogSecondPlayerType);
        final Spinner spDialogThirdPlayerType = (Spinner) container.findViewById(R.id.spDialogThirdPlayerType);
        final Spinner spDialogFourthPlayerType = (Spinner) container.findViewById(R.id.spDialogFourthPlayerType);

        spinner(context, spDialogGameType, Constants.GAME_TYPES);
        spinner(context, spDialogFirstPlayerType, Constants.WHIST_TYPES);
        spinner(context, spDialogSecondPlayerType, Constants.WHIST_TYPES);
        spinner(context, spDialogThirdPlayerType, Constants.WHIST_TYPES);
        spinner(context, spDialogFourthPlayerType, Constants.WHIST_TYPES);


        alert.setView(container);

        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                registerDeal();

                String test = "Pool 1: " + gameRegister.getPlayer(0).getPool().getQuantity() + ", mountain 1: " +
                        gameRegister.getPlayer(0).getMountain().getQuantity() + "whist 1 on 2: " + gameRegister.getPlayer(0).getWhist(1).getQuantity() +
                        "whist 1 on 3: " + gameRegister.getPlayer(0).getWhist(2).getQuantity() + "whist 1 on 4: " + gameRegister.getPlayer(0).getWhist(3).getQuantity();
                Toast.makeText(context, test, Toast.LENGTH_LONG).show();
            }
        });

        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        final AlertDialog alertDialog = alert.show();

        final Button positiveButoon = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);

        positiveButoon.setEnabled(false);

        etDialogFirstPlayerScore.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                int first = 0;
                gameRegister.getPlayer(0).setNumberOfTricks(0);
                if (!s.toString().equals("")) {
                    first = Integer.parseInt(s.toString());
                } else {
                    gameRegister.getPlayer(0).setNumberOfTricks(0);
                }

                if (calcBribeScore(gameRegister.getPlayers()) + first == 10) {
                    positiveButoon.setEnabled(true);
                    titleErrorEnabled[0] = false;
                    tilDialogTitle.setErrorEnabled(false);
                } else {
                    positiveButoon.setEnabled(false);
                    if (!titleErrorEnabled[0]) {
                        tilDialogTitle.setError("   Сумма взяток должна быть равна 10");
                        titleErrorEnabled[0] = true;
                    }
                }

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!s.toString().equals("")) {
                    gameRegister.getPlayer(0).setNumberOfTricks(Integer.parseInt(s.toString()));
                } else {
                    gameRegister.getPlayer(0).setNumberOfTricks(0);
                }
            }
        });

        etDialogSecondPlayerScore.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                int second = 0;
                gameRegister.getPlayer(1).setNumberOfTricks(0);
                if (!s.toString().equals("")) {
                    second = Integer.parseInt(s.toString());
                } else {
                    gameRegister.getPlayer(1).setNumberOfTricks(0);
                }


                if (calcBribeScore(gameRegister.getPlayers()) + second == 10) {
                    positiveButoon.setEnabled(true);
                    titleErrorEnabled[0] = false;
                    tilDialogTitle.setErrorEnabled(false);
                } else {
                    positiveButoon.setEnabled(false);
                    if (!titleErrorEnabled[0]) {
                        tilDialogTitle.setError("   Сумма взяток должна быть равна 10");
                        titleErrorEnabled[0] = true;
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!s.toString().equals("")) {
                    gameRegister.getPlayer(1).setNumberOfTricks(Integer.parseInt(s.toString()));
                } else {
                    gameRegister.getPlayer(1).setNumberOfTricks(0);
                }

            }
        });

        etDialogThirdPlayerScore.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                int third = 0;
                gameRegister.getPlayer(2).setNumberOfTricks(0);
                if (!s.toString().equals("")) {
                    third = Integer.parseInt(s.toString());
                } else {
                    gameRegister.getPlayer(2).setNumberOfTricks(0);
                }


                if (calcBribeScore(gameRegister.getPlayers()) + third == 10) {
                    positiveButoon.setEnabled(true);
                    titleErrorEnabled[0] = false;
                    tilDialogTitle.setErrorEnabled(false);
                } else {
                    positiveButoon.setEnabled(false);
                    if (!titleErrorEnabled[0]) {
                        tilDialogTitle.setError("   Сумма взяток должна быть равна 10");
                        titleErrorEnabled[0] = true;
                    }
                }

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!s.toString().equals("")) {
                    gameRegister.getPlayer(2).setNumberOfTricks(Integer.parseInt(s.toString()));
                } else {
                    gameRegister.getPlayer(2).setNumberOfTricks(0);
                }

            }
        });

        etDialogFourthPlayerScore.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                int fourth = 0;
                gameRegister.getPlayer(3).setNumberOfTricks(0);
                if (!s.toString().equals("")) {
                    fourth = Integer.parseInt(s.toString());
                } else {
                    gameRegister.getPlayer(3).setNumberOfTricks(0);
                }


                if (calcBribeScore(gameRegister.getPlayers()) + fourth == 10) {
                    positiveButoon.setEnabled(true);
                    titleErrorEnabled[0] = false;
                    tilDialogTitle.setErrorEnabled(false);
                } else {
                    positiveButoon.setEnabled(false);
                    if (!titleErrorEnabled[0]) {
                        tilDialogTitle.setError("   Сумма взяток должна быть равна 10");
                        titleErrorEnabled[0] = true;
                    }
                }

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!s.toString().equals("")) {
                    gameRegister.getPlayer(3).setNumberOfTricks(Integer.parseInt(s.toString()));
                } else {
                    gameRegister.getPlayer(3).setNumberOfTricks(0);
                }

            }
        });

        spDialogFirstPlayerType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                setPlayerTypes(gameRegister, position, 0);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spDialogSecondPlayerType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                setPlayerTypes(gameRegister, position, 1);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spDialogThirdPlayerType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                setPlayerTypes(gameRegister, position, 2);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        spDialogFourthPlayerType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                setPlayerTypes(gameRegister, position, 3);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }





    public void showTapDialogThreePlayers(final Context context) {
        final AlertDialog.Builder alert = new AlertDialog.Builder(context);

        ScrollView container = (ScrollView) inflateFrom.getLayoutInflater().inflate(R.layout.dialog_third_field_tap, null);

        final TextInputLayout tilDialogFirstPlayerScore = (TextInputLayout) container.findViewById(R.id.tilDialogFirstPlayerScore);
        final EditText etDialogFirstPlayerScore = tilDialogFirstPlayerScore.getEditText();

        final TextInputLayout tilDialogSecondPlayerScore = (TextInputLayout) container.findViewById(R.id.tilDialogsSecondPlayerScore);
        final EditText etDialogSecondPlayerScore = tilDialogSecondPlayerScore.getEditText();

        final TextInputLayout tilDialogThirdPlayerScore = (TextInputLayout) container.findViewById(R.id.tilDialogsThirdPlayerScore);
        final EditText etDialogThirdPlayerScore = tilDialogThirdPlayerScore.getEditText();


        final TextInputLayout tilDialogTitle = (TextInputLayout) container.findViewById(R.id.tilDialogTitle);

        tilDialogTitle.setError("   Сумма взяток должна быть равна 10");
        final boolean[] titleErrorEnabled = {true};

        gameRegister = GameRegister.getInstance();

        tilDialogFirstPlayerScore.setHint(gameRegister.getPlayer(0).getName());
        tilDialogSecondPlayerScore.setHint(gameRegister.getPlayer(1).getName());
        tilDialogThirdPlayerScore.setHint(gameRegister.getPlayer(2).getName());

        final Spinner spDialogGameType = (Spinner) container.findViewById(R.id.spDialogGameType);
        final Spinner spDialogFirstPlayerType = (Spinner) container.findViewById(R.id.spDialogFirstPlayerType);
        final Spinner spDialogSecondPlayerType = (Spinner) container.findViewById(R.id.spDialogSecondPlayerType);
        final Spinner spDialogThirdPlayerType = (Spinner) container.findViewById(R.id.spDialogThirdPlayerType);

        spinner(context, spDialogGameType, Constants.GAME_TYPES);
        spinner(context, spDialogFirstPlayerType, Constants.WHIST_TYPES);
        spinner(context, spDialogSecondPlayerType, Constants.WHIST_TYPES);
        spinner(context, spDialogThirdPlayerType, Constants.WHIST_TYPES);


        alert.setView(container);

        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                registerDeal();

                String test = "Pool 1: " + gameRegister.getPlayer(0).getPool().getQuantity() + ", mountain 1: " +
                        gameRegister.getPlayer(0).getMountain().getQuantity() + "whist 1 on 2: " + gameRegister.getPlayer(0).getWhist(1).getQuantity() +
                        "whist 1 on 3: " + gameRegister.getPlayer(0).getWhist(2).getQuantity() + "whist 1 on 4: " + gameRegister.getPlayer(0).getWhist(3).getQuantity();
                Toast.makeText(context, test, Toast.LENGTH_LONG).show();
            }
        });

        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        final AlertDialog alertDialog = alert.show();

        final Button positiveButoon = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);

        positiveButoon.setEnabled(false);

        etDialogFirstPlayerScore.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                int first = 0;
                gameRegister.getPlayer(0).setNumberOfTricks(0);
                if (!s.toString().equals("")) {
                    first = Integer.parseInt(s.toString());
                } else {
                    gameRegister.getPlayer(0).setNumberOfTricks(0);
                }

                if (calcBribeScore(gameRegister.getPlayers()) + first == 10) {
                    positiveButoon.setEnabled(true);
                    titleErrorEnabled[0] = false;
                    tilDialogTitle.setErrorEnabled(false);
                } else {
                    positiveButoon.setEnabled(false);
                    if (!titleErrorEnabled[0]) {
                        tilDialogTitle.setError("   Сумма взяток должна быть равна 10");
                        titleErrorEnabled[0] = true;
                    }
                }

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!s.toString().equals("")) {
                    gameRegister.getPlayer(0).setNumberOfTricks(Integer.parseInt(s.toString()));
                } else {
                    gameRegister.getPlayer(0).setNumberOfTricks(0);
                }
            }
        });

        etDialogSecondPlayerScore.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                int second = 0;
                gameRegister.getPlayer(1).setNumberOfTricks(0);
                if (!s.toString().equals("")) {
                    second = Integer.parseInt(s.toString());
                } else {
                    gameRegister.getPlayer(1).setNumberOfTricks(0);
                }


                if (calcBribeScore(gameRegister.getPlayers()) + second == 10) {
                    positiveButoon.setEnabled(true);
                    titleErrorEnabled[0] = false;
                    tilDialogTitle.setErrorEnabled(false);
                } else {
                    positiveButoon.setEnabled(false);
                    if (!titleErrorEnabled[0]) {
                        tilDialogTitle.setError("   Сумма взяток должна быть равна 10");
                        titleErrorEnabled[0] = true;
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!s.toString().equals("")) {
                    gameRegister.getPlayer(1).setNumberOfTricks(Integer.parseInt(s.toString()));
                } else {
                    gameRegister.getPlayer(1).setNumberOfTricks(0);
                }

            }
        });

        etDialogThirdPlayerScore.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                int third = 0;
                gameRegister.getPlayer(2).setNumberOfTricks(0);
                if (!s.toString().equals("")) {
                    third = Integer.parseInt(s.toString());
                } else {
                    gameRegister.getPlayer(2).setNumberOfTricks(0);
                }


                if (calcBribeScore(gameRegister.getPlayers()) + third == 10) {
                    positiveButoon.setEnabled(true);
                    titleErrorEnabled[0] = false;
                    tilDialogTitle.setErrorEnabled(false);
                } else {
                    positiveButoon.setEnabled(false);
                    if (!titleErrorEnabled[0]) {
                        tilDialogTitle.setError("   Сумма взяток должна быть равна 10");
                        titleErrorEnabled[0] = true;
                    }
                }

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!s.toString().equals("")) {
                    gameRegister.getPlayer(2).setNumberOfTricks(Integer.parseInt(s.toString()));
                } else {
                    gameRegister.getPlayer(2).setNumberOfTricks(0);
                }

            }
        });


        spDialogFirstPlayerType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                setPlayerTypes(gameRegister, position, 0);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spDialogSecondPlayerType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                setPlayerTypes(gameRegister, position, 1);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spDialogThirdPlayerType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                setPlayerTypes(gameRegister, position, 2);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }






    private void registerDeal() {
        int numberOfWhisters = 0;
        int playerId = 0;
        for(Player player: gameRegister.getPlayers()) {
            if(player.getStatus() == Player.Status.WHIST) {
                numberOfWhisters++;
            } else if(player.getStatus() == Player.Status.PLAYER) {
                playerId = player.getId();
            }
        }
        gameRegister.onDealConfirmed(DealType.values()[dealTypeNumericValue], playerId, numberOfWhisters);
        //drawScore(); ВАЛИКА АРАРРАРА
        setXML();
    }

    private void setXML() {
        SettingsManager settings = SettingsManager.getInstance();
        settings.putInt("orientation", "left_pool", gameRegister.getPlayer(0).getPool().getQuantity());
        settings.putInt("orientation", "top_pool", gameRegister.getPlayer(1).getPool().getQuantity());
        settings.putInt("orientation", "left_pool", gameRegister.getPlayer(0).getPool().getQuantity());
        settings.putInt("orientation", "left_pool", gameRegister.getPlayer(0).getPool().getQuantity());
    }

    public void spinner(final Context context, Spinner spinner, String[] whist) {

        ArrayAdapter<String> adapter = new DialogFourthPlayersTypesAdapter(context, android.R.layout.simple_spinner_item,
                whist);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                dealTypeNumericValue = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private int calcBribeScore(Player[] players) {

        int bribeScore = 0;

        for (Player aPlayersBribeScore : players) {
            bribeScore += aPlayersBribeScore.getNumberOfTricks();
        }

        return bribeScore;

    }

    private void setPlayerTypes(GameRegister gameRegister, int position, int playerId) {
        switch (position) {
            case 0:
                gameRegister.getPlayer(playerId).setStatus(Player.Status.PASS);
                break;
            case 1:
                gameRegister.getPlayer(playerId).setStatus(Player.Status.WHIST);
                break;
            case 2:
                gameRegister.getPlayer(playerId).setStatus(Player.Status.PLAYER);
                break;
            case 3:
                gameRegister.getPlayer(playerId).setStatus(Player.Status.DEALER);
                break;
        }

    }
}