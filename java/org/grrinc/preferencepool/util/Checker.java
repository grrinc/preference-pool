package org.grrinc.preferencepool.util;

/**
 * Created by tuule on 09.07.15.
 */
public class Checker {
    public static final int RIGHT_POOL = 1;
    public static final int LEFT_POOL = 2;
    public static final int TOP_POOL = 3;
    public static final int BOT_POOL = 4;
    public static final int RIGHT_MOUNTAIN = 5;
    public static final int LEFT_MOUNTAIN = 6;
    public static final int TOP_MOUNTAIN = 7;
    public static final int BOT_MOUNTAIN = 8;
    public static final int CIRCLE = 9;
    boolean isFourPerson;
    private CoordinateHelper mCoordinateHelper;

    public Checker(CoordinateHelper coordinateHelper, boolean isFourPerson) {
        mCoordinateHelper = coordinateHelper;
        this.isFourPerson = isFourPerson;
    }

    public int getClickedFigure(float X, float Y) {
        int res = 0;
        if (isBelongsLeftPool(X, Y))
            res = LEFT_POOL;
        else if (isBelongsCircle(X, Y))
            res = CIRCLE;
        else if (isBelongsLeftMountain(X, Y))
            res = LEFT_MOUNTAIN;
        else if (isBelongsRightPool(X, Y))
            res = RIGHT_POOL;
        else if (isBelongsRightMountain(X, Y))
            res = RIGHT_MOUNTAIN;
        else if (isBelongsBotPool(X, Y))
            res = BOT_POOL;
        else if (isBelongsBotMountain(X, Y))
            res = BOT_MOUNTAIN;
        else if(isFourPerson) {
            if (isBelongsTopPool(X, Y))
                res = TOP_POOL;
            else if (isBelongsTopMountain(X, Y))
                res = TOP_MOUNTAIN;
        }
        return res;
    }

    private boolean isBelongsLeftPool(float X, float Y) {
        boolean res = false;
        float lineCoefficient = mCoordinateHelper.getY() / mCoordinateHelper.getX();
        float rightLaneX = 0;
        float leftLaneX = mCoordinateHelper.getMountainX();
        float topLaneY;
        if (isFourPerson) {
            topLaneY = lineCoefficient * X + 0.0087f * mCoordinateHelper.getY();
        }
        else {
            topLaneY = 0;
        }

        float botLaneY = -lineCoefficient * X + mCoordinateHelper.getY() - 0.0087f * mCoordinateHelper.getY();
        if ((X > rightLaneX) && (X < leftLaneX) && (Y > topLaneY) && (Y < botLaneY))
            res = true;
        return res;
    }

    private boolean isBelongsLeftMountain(float X, float Y) {
        boolean res = false;
        float lineCoefficient = mCoordinateHelper.getY() / mCoordinateHelper.getX();
        float rightLaneX = mCoordinateHelper.getMountainX();
        float leftLaneX = mCoordinateHelper.getCenterX() - mCoordinateHelper.getR();
        float topLaneY;
        if (isFourPerson) {
            topLaneY = lineCoefficient * X;
        }
        else {
            topLaneY = 0;
        }
        float botLaneY = -lineCoefficient * X + mCoordinateHelper.getY();
        if ((X > rightLaneX) && (X < leftLaneX) && (Y > topLaneY) && (Y < botLaneY))
            res = true;
        return res;
    }

    private boolean isBelongsRightPool(float X, float Y) {
        boolean res = false;
        float lineCoefficient = mCoordinateHelper.getY() / mCoordinateHelper.getX();
        float rightLaneX = mCoordinateHelper.getX() - mCoordinateHelper.getMountainX();
        float leftLaneX = mCoordinateHelper.getX();
        float botLaneY = lineCoefficient * X + 0.0087f * mCoordinateHelper.getY();
        float topLaneY;
        if (isFourPerson) {
            topLaneY = -lineCoefficient * X + mCoordinateHelper.getY() - 0.0087f * mCoordinateHelper.getY();
        }
        else {
            topLaneY = 0;
        }
        if ((X > rightLaneX) && (X < leftLaneX) && (Y > topLaneY) && (Y < botLaneY))
            res = true;
        return res;
    }

    private boolean isBelongsRightMountain(float X, float Y) {
        boolean res = false;
        float lineCoefficient = mCoordinateHelper.getY() / mCoordinateHelper.getX();
        float leftLaneX = mCoordinateHelper.getX() - mCoordinateHelper.getMountainX();
        float rightLaneX = mCoordinateHelper.getCenterX() + mCoordinateHelper.getR();
        float botLaneY = lineCoefficient * X + 0.0087f * mCoordinateHelper.getY();
        float topLaneY;
        if (isFourPerson) {
            topLaneY = -lineCoefficient * X + mCoordinateHelper.getY() - 0.0087f * mCoordinateHelper.getY();
        }
        else {
            topLaneY = 0;
        }
        if ((X > rightLaneX) && (X < leftLaneX) && (Y > topLaneY) && (Y < botLaneY))
            res = true;
        return res;
    }

    private boolean isBelongsBotPool(float X, float Y) {
        boolean res = false;
        float lineCoefficient = mCoordinateHelper.getX() / mCoordinateHelper.getY();
        float leftLaneX = lineCoefficient * Y + 0.0087f * mCoordinateHelper.getX();
        float rightLaneX = -lineCoefficient * Y + mCoordinateHelper.getX() - 0.0087f * mCoordinateHelper.getX();
        float botLaneY = mCoordinateHelper.getY();
        float topLaneY = mCoordinateHelper.getMountainY();
        if ((X > rightLaneX) && (X < leftLaneX) && (Y > topLaneY) && (Y < botLaneY))
            res = true;
        return res;
    }

    private boolean isBelongsBotMountain(float X, float Y) {
        boolean res = false;
        float lineCoefficient = mCoordinateHelper.getX() / mCoordinateHelper.getY();
        float leftLaneX = lineCoefficient * Y + 0.0087f * mCoordinateHelper.getX();
        float rightLaneX = -lineCoefficient * Y + mCoordinateHelper.getX() - 0.0087f * mCoordinateHelper.getX();
        float botLaneY = mCoordinateHelper.getMountainY();
        float topLaneY = mCoordinateHelper.getCenterY() + mCoordinateHelper.getR();
        if ((X > rightLaneX) && (X < leftLaneX) && (Y > topLaneY) && (Y < botLaneY))
            res = true;
        return res;
    }

    private boolean isBelongsTopPool(float X, float Y) {
        boolean res = false;
        float lineCoefficient = mCoordinateHelper.getX() / mCoordinateHelper.getY();
        float rightLaneX = lineCoefficient * Y + 0.0087f * mCoordinateHelper.getX();
        float leftLaneX = -lineCoefficient * Y + mCoordinateHelper.getX() - 0.0087f * mCoordinateHelper.getX();
        float botLaneY = mCoordinateHelper.getY() - mCoordinateHelper.getMountainY();
        float topLaneY = 0;
        if ((X > rightLaneX) && (X < leftLaneX) && (Y > topLaneY) && (Y < botLaneY))
            res = true;
        return res;
    }

    private boolean isBelongsTopMountain(float X, float Y) {
        boolean res = false;
        float lineCoefficient = mCoordinateHelper.getX() / mCoordinateHelper.getY();
        float rightLaneX = lineCoefficient * Y + 0.0087f * mCoordinateHelper.getX();
        float leftLaneX = -lineCoefficient * Y + mCoordinateHelper.getX() - 0.0087f * mCoordinateHelper.getX();
        float topLaneY = mCoordinateHelper.getY() - mCoordinateHelper.getMountainY();
        float botLaneY = mCoordinateHelper.getCenterY() - mCoordinateHelper.getR();
        if ((X > rightLaneX) && (X < leftLaneX) && (Y > topLaneY) && (Y < botLaneY))
            res = true;
        return res;
    }

    private boolean isBelongsCircle(float X, float Y) {
        boolean res = false;
        float topLaneY = (float) (mCoordinateHelper.getCenterY() - Math.sqrt(Math.pow(mCoordinateHelper.getR(), 2) - Math.pow((X - mCoordinateHelper.getCenterX()), 2)));
        float botLaneY = (float) (mCoordinateHelper.getCenterY() + Math.sqrt(Math.pow(mCoordinateHelper.getR(), 2) - Math.pow((X - mCoordinateHelper.getCenterX()), 2)));
        float leftLaneX = (float) (mCoordinateHelper.getCenterX() - Math.sqrt(Math.pow(mCoordinateHelper.getR(), 2) - Math.pow((Y - mCoordinateHelper.getCenterY()), 2)));
        float rightLaneX = (float) (mCoordinateHelper.getCenterX() + Math.sqrt(Math.pow(mCoordinateHelper.getR(), 2) - Math.pow((Y - mCoordinateHelper.getCenterY()), 2)));

        if ((X > leftLaneX) && (X < rightLaneX) && (Y > topLaneY) && (Y < botLaneY))
            res = true;
        return res;
    }
}

