package org.grrinc.preferencepool.util.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import org.grrinc.preferencepool.GameRegister;
import org.grrinc.preferencepool.Player;




public class DialogFourthPlayersTypesAdapter extends ArrayAdapter<String> {

    private GameRegister gameRegister;
    public DialogFourthPlayersTypesAdapter(
            Context context, int textViewResId, String[] strings) {
        super(context, textViewResId, strings);
        this.gameRegister = GameRegister.getInstance();

    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return super.getView(position, convertView, parent);
    }

    @Override
    public boolean areAllItemsEnabled() {
        return false;
    }


    @Override
    public boolean isEnabled(int position) {

        boolean gamerAlreadyExist = false;
        boolean dealerAlreadyExist = false;

        for (int i = 0; i < 4; i++) {
            if (gameRegister.getPlayer(i).getStatus() == Player.Status.PLAYER) {
                gamerAlreadyExist = true;
            }
            if (gameRegister.getPlayer(i).getStatus() == Player.Status.DEALER) {
                dealerAlreadyExist = true;
            }

        }

        if (position == 2 && gamerAlreadyExist) {
            return false;
        } else if (position == 3 && dealerAlreadyExist) {
            return false;
        } else {
            return true;
        }




    }




}