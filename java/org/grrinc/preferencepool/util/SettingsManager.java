package org.grrinc.preferencepool.util;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by wetalkas on 18.06.15.
 */
public class SettingsManager {
    private static SettingsManager instance;
    private Context context;

    public static final String GLOBAL_SETTINGS_FIELD = "org.grrinc.preferencepool_preferences";

    public static final String SETTINGS_FIELD = "settings";



    private SettingsManager() {
    }

    public static SettingsManager getInstance() {
        if (instance == null) {
            instance = new SettingsManager();
        }
        return instance;
    }


    public void init(Context context){
        this.context = context;
    }


    public String getString(String field, String key) {
        return context.getSharedPreferences(field, Context.MODE_PRIVATE).getString(key, null);
    }

    public void putString(String field, String key, String value) {
        SharedPreferences preferences = context.getSharedPreferences(field, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key, value);
        editor.apply();
    }


    public boolean getBoolean(String field, String key) {
        return context.getSharedPreferences(field, Context.MODE_PRIVATE).getBoolean(key, false);
    }

    public void putBoolean(String field, String key, boolean value) {
        SharedPreferences preferences = context.getSharedPreferences(field, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }


    public int getInt(String field, String key) {
        return context.getSharedPreferences(field, Context.MODE_PRIVATE).getInt(key, 0);
    }

    public void putInt(String field, String key, int value) {
        SharedPreferences preferences = context.getSharedPreferences(field, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(key, value);
        editor.apply();
    }


    public long getLong(String field, String key) {
        return context.getSharedPreferences(field, Context.MODE_PRIVATE).getLong(key, 0);
    }

    public void putLong(String field, String key, long value) {
        SharedPreferences preferences = context.getSharedPreferences(field, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putLong(key, value);
        editor.apply();
    }


    public float getFloat(String field, String key) {
        return context.getSharedPreferences(field, Context.MODE_PRIVATE).getFloat(key, 0);
    }

    public void putFloat(String field, String key, float value) {
        SharedPreferences preferences = context.getSharedPreferences(field, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putFloat(key, value);
        editor.apply();
    }


}
