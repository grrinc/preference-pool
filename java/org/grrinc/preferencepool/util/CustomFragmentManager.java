package org.grrinc.preferencepool.util;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;

public class CustomFragmentManager {

    private static CustomFragmentManager instance;

    private FragmentManager fragmentManager;

    private Activity activity;

    private CustomFragmentManager() {

    }

    public static CustomFragmentManager getInstance() {
        if (instance == null) {
            instance = new CustomFragmentManager();
        }
        return instance;
    }


    public void init(Activity activity){
        this.activity = activity;
        fragmentManager = activity.getFragmentManager();
    }


    public void setFragment(int containerViewId, Fragment fragment, boolean addToBackStack) {
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(containerViewId, fragment);

        if (addToBackStack) {
            fragmentTransaction.addToBackStack(null);
        }
        fragmentTransaction.commit();
    }


    public FragmentManager getFragmentManager() {
        return fragmentManager;
    }

}
