package org.grrinc.preferencepool.util;

/**
 * Created by guguza on 03.07.2015.
 */

public enum ScoreBridge {
    LEFT_POOL("2.4.8LP"), TOP_POOL("2.6.10TP"), RIGHT_POOL("2.10RP"), BOTTOM_POOL("2BP"),
    LEFT_MOUNTAIN("2.4LM"), TOP_MOUNTAIN("2TM"), RIGHT_MOUNTAIN("8RM"), BOTTOM_MOUNTAIN("6.12BM"),
    LEFT_WHIST_ON_TOP("4.8.22LT"), LEFT_WHIST_ON_RIGHT("2.8.20LR"), LEFT_WHIST_ON_BOTTOM("8.10LB"),
    RIGHT_WHIST_ON_LEFT("2.4RL"), RIGHT_WHIST_ON_TOP("2.10.20RT"), RIGHT_WHIST_ON_BOTTOM("8.18RB"),
    TOP_WHIST_ON_LEFT("4.8.10TL"), TOP_WHIST_ON_RIGHT("2.10TR"), TOP_WHIST_ON_BOTTOM("8.10.14TB"),
    BOTTOM_WHIST_ON_LEFT("4.24BL"), BOTTOM_WHIST_ON_TOP("2.22BT"), BOTTOM_WHIST_ON_RIGHT("8.28BR");


    ScoreBridge(String score) {
        this.setScore(score);
    }

    private String score;

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public void addScore(String score) {
        this.score += ". " + score;
    }
}

