package org.grrinc.preferencepool.util;

/**
 * Created by tuule on 18.06.15.
 */
public class CoordinateHelper {
    private int X,Y;

    public float getR() {
        return R;
    }

    private float R;
    private double angle;
    private double poolWidthCoeff;
    private double whistWideness;
    private double mountainWidthCoeff;

    public CoordinateHelper(int screenX, int screenY, float radius, double poolWidth, double whistWide, double mountainWidth){
        X=screenX;
        Y=screenY;
        R=radius;
        angle = Math.atan(Y/X);

        poolWidthCoeff = poolWidth;
        whistWideness = whistWide;
        mountainWidthCoeff = mountainWidth;
    }
    public float getX(){
        return (float) X;
    }
    public float getY(){
        return (float) Y;
    }
    public float getCenterX(){
        return X/2;
    }
    public float getCenterY(){
        return Y/2;
    }
    public float[][] getLeftTopCoords(){
        float[][] res = new float[2][2];
        res[0][0] = (float) (getCenterX() - R * Math.cos(angle));
        res[1][0] = (float) (getCenterX() + R * Math.cos(angle));
        res[0][1] = (float) (getCenterY() - R * Math.sin(angle));
        res[1][1] = (float) (getCenterY() + R * Math.sin(angle));
        return res;
    }
    public float[][] getRightTopCoords(){
        float[][] res = new float[2][2];
        res[0][0] = (float) (getCenterX() - R * Math.cos(-angle));
        res[1][0] = (float) (getCenterX() + R * Math.cos(-angle));
        res[0][1] = (float) (getCenterY() - R * Math.sin(-angle));
        res[1][1] = (float) (getCenterY() + R * Math.sin(-angle));
        return res;
    }

    public float getMountainX(){
        return (float) (getRightTopCoords()[1][0] * mountainWidthCoeff);
    }
    public float getMountainY(){
        return getY() -
                (getMountainX() + 0.0121f * getX()) * Y / X;
    }
    public float getPoolX() {

        return (float) (getRightTopCoords()[1][0] * poolWidthCoeff);
    }
    public float getPoolY(){
        return getY() -
                (getPoolX() + 0.0083f * getX()) * Y / X;
    }
    public float getWhistX(){
        return (float) (getCenterX() - getCenterX() * whistWideness);
    }
    public float getWhistY(){
        return (float) (getCenterY() - getCenterY() * whistWideness);
    }
    public float getWhistEndX(){
        return (float) (getCenterX() - getCenterX() * 1.6 * whistWideness);
    }
    public float getWhistEndY(){
        return (float) (getCenterY() - getCenterY() * 1.6 * whistWideness);
    }


}
