package org.grrinc.preferencepool.util;

import android.graphics.Canvas;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

/**
 * Created by tuule on 18.06.15.
 */
 public class CanvasThread extends Thread {
        private SurfaceHolder _holder;
        private SurfaceView _surfaceView;
        private boolean _run = false;
        private boolean _started;

        public CanvasThread(SurfaceHolder surfaceHolder, SurfaceView surfaceView) {
            _holder = surfaceHolder;
            _surfaceView = surfaceView;
        }

        public void setRunning(boolean run) {
            _run = run;
        }

        public boolean isRunning(){
            return _run;
        }
        public boolean isStarted() {return  _started;}
        @Override
        public void run() {
            Canvas canvas;
            _started = true;
            while (_run) {
                canvas = null;
                try {
                    canvas = _holder.lockCanvas(null);
                    synchronized (_holder) {
                        _surfaceView.draw(canvas);
                    }
                } finally {
                    if (canvas != null) {
                        _holder.unlockCanvasAndPost(canvas);
                    }
                }
            }
        }
 }
