package org.grrinc.preferencepool.util;

/**
 * Created by wetalkas on 27.06.15.
 */
public class Constants {

    public static final String[] GAME_TYPES = {"Шестерная", "Семерная", "Восьмерная", "Распасы",
            "Девятерная", "Десятерная", "Мизер"};
    public static final String[] WHIST_TYPES = {"Пас", "Вист", "Игрок", "Раздающий"};
}
