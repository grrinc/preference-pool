package org.grrinc.preferencepool;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import org.grrinc.preferencepool.activity.PupsenActivity;
import org.grrinc.preferencepool.logic.Game;
import org.grrinc.preferencepool.util.CanvasThread;
import org.grrinc.preferencepool.util.Checker;
import org.grrinc.preferencepool.util.CoordinateHelper;
import org.grrinc.preferencepool.util.Dialogs;
import org.grrinc.preferencepool.util.ScoreBridge;
import org.grrinc.preferencepool.util.SettingsManager;

/**
 * Created by tuule__ on 22.06.15.
 */
@SuppressLint("ViewConstructor")
public class GameGeometry extends SurfaceView implements SurfaceHolder.Callback {

    private CanvasThread canvasThread;
    private Paint paint;
    private Paint textPaint;
    private CoordinateHelper coordinateHelper;
    private GestureDetector gestureDetector;
    private GameRegister gameRegister;
    private PupsenActivity pupsenActivity;
    private Context mContext;
    private Dialogs dialogs;
    private SettingsManager settingsManager = SettingsManager.getInstance();
    private Canvas mCanvas;


    public GameGeometry(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        mContext = context;

        canvasThread = new CanvasThread(getHolder(), this);
        gestureDetector = new GestureDetector(context, new GestureListener());

        getHolder().addCallback(this);
        setFocusable(true);

        gameRegister = GameRegister.getInstance();

        pupsenActivity = (PupsenActivity) mContext;

        dialogs = new Dialogs(pupsenActivity);

        paint = new Paint();
        paint.setColor(Color.BLACK);
        paint.setAntiAlias(true);
        paint.setStrokeWidth(3);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeJoin(Paint.Join.ROUND);
        paint.setStrokeCap(Paint.Cap.ROUND);

        textPaint = new Paint();
        textPaint.setColor(Color.BLACK);
        textPaint.setAntiAlias(true);
        textPaint.setStyle(Paint.Style.FILL);

        int numberOfPlayers;
        if (isFourPerson()) {
            numberOfPlayers = 4;
        } else {
            numberOfPlayers = 3;
        }
        setGameParams(numberOfPlayers);
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        canvasThread.setRunning(true);
        if (!canvasThread.isStarted())
            canvasThread.start();
        else {
            canvasThread.run();
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        boolean retry = true;
        canvasThread.setRunning(false);
        while (retry) {
            try {
                canvasThread.join();
                retry = false;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void draw(@NonNull Canvas canvas) {
        //some vars init
        mCanvas = canvas;
        float radius = getWidth() * 0.06f;
        coordinateHelper = new CoordinateHelper(this.getWidth(), this.getHeight(), radius, 0.3, 0.25, 0.55);
        float fontSize = (float) (coordinateHelper.getY() * 0.034);
        textPaint.setTextSize(fontSize);

        //4-player pool's skeleton drawing
        canvas.drawColor(Color.WHITE);
        drawPoolSkeleton(canvas, radius, isFourPerson());

        Log.d("canvas", "redrawing!");

        //single changes
        drawScore();
        //disable thread for resources economy
        canvasThread.setRunning(false);
    }

    private void drawPoolSkeleton(Canvas canvas, float radius, boolean isFourPerson) {

        //circle&cross draw
        canvas.drawCircle(coordinateHelper.getCenterX(), coordinateHelper.getCenterY(), radius,
                paint);
        canvas.drawLine(coordinateHelper.getX(), coordinateHelper.getY(), coordinateHelper.getLeftTopCoords()[1][0], coordinateHelper.getLeftTopCoords()[1][1], paint);
        canvas.drawLine(0, coordinateHelper.getY(), coordinateHelper.getRightTopCoords()[0][0], coordinateHelper.getRightTopCoords()[0][1], paint);

        if (isFourPerson) {
            drawFourPersonPool(canvas);
        } else {
            drawThreePersonPool(canvas);
        }

    }

    private void setGameParams(int numberOfPlayers) {
        String[] playersName = new String[numberOfPlayers];
        for (int i = 0; i < playersName.length; i++) {
            playersName[i] = "Playerrr " + (i + 1);
        }
        //let it be SOCHI as first example

        gameRegister.onGameSet(Game.GameType.SOCHI, numberOfPlayers, playersName);
    }

    //TODO: использование индексов первого и последнего символа везде
    public void drawScore() {
        if (!canvasThread.isRunning())
            canvasThread.setRunning(true);
        else {
            if (!isFourPerson()) {
                mCanvas.save();
                mCanvas.rotate(90, (float) (coordinateHelper.getPoolX() +
                                coordinateHelper.getPoolX() * 0.3),
                        (float) (coordinateHelper.getY() * 0.01));
                mCanvas.drawText(ScoreBridge.LEFT_POOL.getScore(),
                        (float) (coordinateHelper.getPoolX() +
                                coordinateHelper.getPoolX() * 0.3),
                        (float) (coordinateHelper.getY() * 0.01), textPaint);
                mCanvas.restore();

                mCanvas.save();
                mCanvas.rotate(90, (float) (coordinateHelper.getMountainX() +
                                coordinateHelper.getMountainX() * 0.3),
                        (float) (coordinateHelper.getY() * 0.01));
                mCanvas.drawText(ScoreBridge.LEFT_MOUNTAIN.getScore(),
                        (float) (coordinateHelper.getMountainX() +
                                coordinateHelper.getMountainX() * 0.3),
                        (float) (coordinateHelper.getY() * 0.01), textPaint);
                mCanvas.restore();

                mCanvas.save();
                mCanvas.rotate(90, (float) (coordinateHelper.getMountainX() +
                                coordinateHelper.getMountainX() * 0.3),
                        (float) (coordinateHelper.getY() * 0.01));
                mCanvas.drawText(ScoreBridge.LEFT_MOUNTAIN.getScore(),
                        (float) (coordinateHelper.getMountainX() +
                                coordinateHelper.getMountainX() * 0.3),
                        (float) (coordinateHelper.getY() * 0.01), textPaint);
                mCanvas.restore();

                mCanvas.save();
                mCanvas.rotate(90, (float) (coordinateHelper.getPoolX() -
                                coordinateHelper.getPoolX() * 0.63),
                        (float) (coordinateHelper.getY() * 0.01));
                mCanvas.drawText(ScoreBridge.LEFT_WHIST_ON_RIGHT.getScore(),
                        (float) (coordinateHelper.getPoolX() -
                                coordinateHelper.getPoolX() * 0.63),
                        (float) (coordinateHelper.getY() * 0.01), textPaint);
                mCanvas.restore();

                mCanvas.save();
                mCanvas.rotate(90, (float) (coordinateHelper.getPoolX() -
                                coordinateHelper.getPoolX() * 0.63),
                        (float) (coordinateHelper.getY() * 0.01 +
                                coordinateHelper.getPoolY() / 2));
                mCanvas.drawText(ScoreBridge.LEFT_WHIST_ON_BOTTOM.getScore(),
                        (float) (coordinateHelper.getPoolX() -
                                coordinateHelper.getPoolX() * 0.63),
                        (float) (coordinateHelper.getY() * 0.01 + coordinateHelper.getPoolY() / 2), textPaint);
                mCanvas.restore();

                mCanvas.save();
                mCanvas.rotate(270, (float) (coordinateHelper.getX() -
                                coordinateHelper.getPoolX() -
                                coordinateHelper.getPoolX() * 0.3),
                        (float) (((coordinateHelper.getPoolY() +
                                coordinateHelper.getMountainY()) / 2) -
                                coordinateHelper.getY() * 0.05));
                mCanvas.drawText(ScoreBridge.RIGHT_POOL.getScore(),
                        (float) (coordinateHelper.getX() - coordinateHelper.getPoolX() -
                                coordinateHelper.getPoolX() * 0.3),
                        (float) (((coordinateHelper.getPoolY() +
                                coordinateHelper.getMountainY()) / 2) -
                                coordinateHelper.getY() * 0.05), textPaint);
                mCanvas.restore();

                mCanvas.save();
                mCanvas.rotate(270, (float) (coordinateHelper.getX() -
                                coordinateHelper.getPoolX() +
                                coordinateHelper.getPoolX() * 0.6),
                        (float) (coordinateHelper.getPoolY() +
                                coordinateHelper.getY() * 0.04));
                mCanvas.drawText(ScoreBridge.RIGHT_WHIST_ON_BOTTOM.getScore(),
                        (float) (coordinateHelper.getX() - coordinateHelper.getPoolX() +
                                coordinateHelper.getPoolX() * 0.6),
                        (float) (coordinateHelper.getPoolY() +
                                coordinateHelper.getY() * 0.04), textPaint);
                mCanvas.restore();

                mCanvas.save();
                mCanvas.rotate(270, (float) (coordinateHelper.getX() -
                                coordinateHelper.getPoolX() +
                                coordinateHelper.getPoolX() * 0.6),
                        (float) (coordinateHelper.getPoolY() / 2 - coordinateHelper.getY() * 0.01));
                mCanvas.drawText(ScoreBridge.RIGHT_WHIST_ON_LEFT.getScore(),
                        (float) (coordinateHelper.getX() - coordinateHelper.getPoolX() +
                                coordinateHelper.getPoolX() * 0.6),
                        (float) (coordinateHelper.getPoolY() / 2 - coordinateHelper.getY() * 0.01), textPaint);
                mCanvas.restore();

                mCanvas.save();
                mCanvas.rotate(270, (float) (coordinateHelper.getX() -
                                coordinateHelper.getMountainX() -
                                coordinateHelper.getMountainX() * 0.2),
                        (float) (((coordinateHelper.getPoolY() +
                                coordinateHelper.getMountainY()) / 2) -
                                coordinateHelper.getY() * 0.2));
                mCanvas.drawText(ScoreBridge.RIGHT_MOUNTAIN.getScore(),
                        (float) (coordinateHelper.getX() - coordinateHelper.getMountainX() -
                                coordinateHelper.getMountainX() * 0.2),
                        (float) (((coordinateHelper.getPoolY() +
                                coordinateHelper.getMountainY()) / 2) -
                                coordinateHelper.getY() * 0.2), textPaint);
                mCanvas.restore();

                mCanvas.drawText(ScoreBridge.BOTTOM_WHIST_ON_LEFT.getScore(),
                        coordinateHelper.getPoolX(),
                        (float) (coordinateHelper.getPoolY() + coordinateHelper.getY() * 0.05),
                        textPaint);

                mCanvas.drawText(ScoreBridge.BOTTOM_WHIST_ON_RIGHT.getScore(),
                        (float) (coordinateHelper.getCenterX() + coordinateHelper.getY() * 0.01),
                        (float) (coordinateHelper.getPoolY() + coordinateHelper.getY() * 0.05),
                        textPaint);
            }
            mCanvas.drawText(ScoreBridge.BOTTOM_POOL.getScore(),
                    coordinateHelper.getMountainX(),
                    (float) (coordinateHelper.getMountainY() + coordinateHelper.getY() * 0.05),
                    textPaint);

            //динамическая отрисовка строки с границами: первые 4, затем -- остальные с новой "строки"
            mCanvas.drawText(ScoreBridge.BOTTOM_MOUNTAIN.getScore(), 0, 4,
                    (float) (coordinateHelper.getCenterX() - coordinateHelper.getX() * 0.06),
                    (float) (coordinateHelper.getCenterY() + coordinateHelper.getY() * 0.08),
                    textPaint);

            if (ScoreBridge.BOTTOM_MOUNTAIN.getScore().length() >= 4 ) {
                mCanvas.drawText(ScoreBridge.BOTTOM_MOUNTAIN.getScore(), 4,
                        ScoreBridge.BOTTOM_MOUNTAIN.getScore().length(),
                        (float) (coordinateHelper.getCenterX() - coordinateHelper.getX() * 0.06
                        - coordinateHelper.getX() * 0.04),
                        (float) (coordinateHelper.getCenterY() + coordinateHelper.getY() * 0.12),
                        textPaint);
            }



        }
    }

    private void drawThreePersonPool(Canvas canvas) {
        canvas.drawLine(coordinateHelper.getCenterX(), coordinateHelper.getCenterY() - coordinateHelper.getR(), coordinateHelper.getCenterX(), 0, paint);
        canvas.drawLine(coordinateHelper.getMountainX(), coordinateHelper.getMountainY(), coordinateHelper.getX() - coordinateHelper.getMountainX(), coordinateHelper.getMountainY(), paint);
        canvas.drawLine(coordinateHelper.getMountainX(), coordinateHelper.getMountainY(), coordinateHelper.getMountainX(), 0, paint);
        canvas.drawLine(coordinateHelper.getX() - coordinateHelper.getMountainX(), coordinateHelper.getMountainY(), coordinateHelper.getX() - coordinateHelper.getMountainX(), 0, paint);
        canvas.drawLine(coordinateHelper.getCenterX(), coordinateHelper.getCenterY() - coordinateHelper.getR(), coordinateHelper.getCenterX(), 0, paint);
        canvas.drawLine(coordinateHelper.getPoolX(), coordinateHelper.getPoolY(), coordinateHelper.getX() - coordinateHelper.getPoolX(), coordinateHelper.getPoolY(), paint);
        canvas.drawLine(coordinateHelper.getPoolX(), coordinateHelper.getPoolY(), coordinateHelper.getPoolX(), 0, paint);
        canvas.drawLine(coordinateHelper.getX() - coordinateHelper.getPoolX(), coordinateHelper.getPoolY(), coordinateHelper.getX() - coordinateHelper.getPoolX(), 0, paint);
        canvas.drawLine(coordinateHelper.getCenterX(), coordinateHelper.getPoolY(), coordinateHelper.getCenterX(), coordinateHelper.getY(), paint);
        canvas.drawLine(coordinateHelper.getPoolX(), coordinateHelper.getPoolY() / 2, 0, coordinateHelper.getPoolY() / 2, paint);
        canvas.drawLine(coordinateHelper.getX() - coordinateHelper.getPoolX(), coordinateHelper.getPoolY() / 2, coordinateHelper.getX(), coordinateHelper.getPoolY() / 2, paint);

    }

    private void drawFourPersonPool(Canvas canvas) {
        canvas.drawLine(0, 0, coordinateHelper.getLeftTopCoords()[0][0], coordinateHelper.getLeftTopCoords()[0][1], paint);
        canvas.drawLine(coordinateHelper.getX(), 0, coordinateHelper.getRightTopCoords()[1][0], coordinateHelper.getRightTopCoords()[1][1], paint);
        //mountain box draw
        canvas.drawLine(coordinateHelper.getMountainX(), coordinateHelper.getMountainY(), coordinateHelper.getMountainX(), coordinateHelper.getY() - coordinateHelper.getMountainY(), paint);
        canvas.drawLine(coordinateHelper.getMountainX(), coordinateHelper.getMountainY(), coordinateHelper.getX() - coordinateHelper.getMountainX(), coordinateHelper.getMountainY(), paint);
        canvas.drawLine(coordinateHelper.getX() - coordinateHelper.getMountainX(), coordinateHelper.getMountainY(), coordinateHelper.getX() - coordinateHelper.getMountainX(), coordinateHelper.getY() - coordinateHelper.getMountainY(), paint);
        canvas.drawLine(coordinateHelper.getX() - coordinateHelper.getMountainX(), coordinateHelper.getY() - coordinateHelper.getMountainY(), coordinateHelper.getMountainX(), coordinateHelper.getY() - coordinateHelper.getMountainY(), paint);

        //pool box draw
        canvas.drawLine(coordinateHelper.getPoolX(), coordinateHelper.getPoolY(), coordinateHelper.getPoolX(), coordinateHelper.getY() - coordinateHelper.getPoolY(), paint);
        canvas.drawLine(coordinateHelper.getPoolX(), coordinateHelper.getPoolY(), coordinateHelper.getX() - coordinateHelper.getPoolX(), coordinateHelper.getPoolY(), paint);
        canvas.drawLine(coordinateHelper.getX() - coordinateHelper.getPoolX(), coordinateHelper.getPoolY(), coordinateHelper.getX() - coordinateHelper.getPoolX(), coordinateHelper.getY() - coordinateHelper.getPoolY(), paint);
        canvas.drawLine(coordinateHelper.getX() - coordinateHelper.getPoolX(), coordinateHelper.getY() - coordinateHelper.getPoolY(), coordinateHelper.getPoolX(), coordinateHelper.getY() - coordinateHelper.getPoolY(), paint);

        //whist separator lines draw
        canvas.drawLine(coordinateHelper.getWhistX(), coordinateHelper.getPoolY(), coordinateHelper.getWhistEndX(), coordinateHelper.getY(), paint);
        canvas.drawLine(coordinateHelper.getX() - coordinateHelper.getWhistX(), coordinateHelper.getPoolY(), coordinateHelper.getX() - coordinateHelper.getWhistEndX(), coordinateHelper.getY(), paint);
        canvas.drawLine(coordinateHelper.getWhistX(), coordinateHelper.getY() - coordinateHelper.getPoolY(), coordinateHelper.getWhistEndX(), 0, paint);
        canvas.drawLine(coordinateHelper.getX() - coordinateHelper.getWhistX(), coordinateHelper.getY() - coordinateHelper.getPoolY(), coordinateHelper.getX() - coordinateHelper.getWhistEndX(), 0, paint);
        canvas.drawLine(coordinateHelper.getPoolX(), coordinateHelper.getWhistY(), 0, coordinateHelper.getWhistEndY(), paint);
        canvas.drawLine(coordinateHelper.getPoolX(), coordinateHelper.getY() - coordinateHelper.getWhistY(), 0, coordinateHelper.getY() - coordinateHelper.getWhistEndY(), paint);
        canvas.drawLine(coordinateHelper.getX() - coordinateHelper.getPoolX(), coordinateHelper.getWhistY(), coordinateHelper.getX(), coordinateHelper.getWhistEndY(), paint);
        canvas.drawLine(coordinateHelper.getX() - coordinateHelper.getPoolX(), coordinateHelper.getY() - coordinateHelper.getWhistY(), coordinateHelper.getX(), coordinateHelper.getY() - coordinateHelper.getWhistEndY(), paint);
    }


    private boolean isFourPerson() {
        boolean isFourPerson;
        settingsManager.init(mContext);
        //TODO: переписать, а то как лохи
        try {
            isFourPerson = !settingsManager.getString(SettingsManager.GLOBAL_SETTINGS_FIELD, "number_of_players").equals("3");
        } catch (Exception e) {
            isFourPerson = false;
        }
        return isFourPerson;
    }

    @Override
    public boolean onTouchEvent(@NonNull MotionEvent event) {
        return gestureDetector.onTouchEvent(event);
    }


    private class GestureListener extends GestureDetector.SimpleOnGestureListener {

        @Override
        public boolean onDown(MotionEvent e) {
            return true;
        }

        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) {

            Checker checker = new Checker(coordinateHelper, isFourPerson());

            int score = 0;
            int field = 0;

            switch (checker.getClickedFigure(e.getX(), e.getY())) {
                case Checker.LEFT_POOL:
                    Toast.makeText(mContext, "Left Pool clicked", Toast.LENGTH_SHORT).show();
                    field = Checker.LEFT_POOL;
                    break;
                case Checker.LEFT_MOUNTAIN:
                    Toast.makeText(mContext, "Left Mountain clicked", Toast.LENGTH_SHORT).show();
                    field = Checker.LEFT_MOUNTAIN;
                    break;
                case Checker.RIGHT_POOL:
                    Toast.makeText(mContext, "Right Pool clicked", Toast.LENGTH_SHORT).show();
                    field = Checker.RIGHT_POOL;
                    break;
                case Checker.RIGHT_MOUNTAIN:
                    Toast.makeText(mContext, "Right Mountain clicked", Toast.LENGTH_SHORT).show();
                    field = Checker.RIGHT_MOUNTAIN;
                    break;
                case Checker.BOT_POOL:
                    Toast.makeText(mContext, "Bottom Pool clicked", Toast.LENGTH_SHORT).show();
                    field = Checker.BOT_POOL;
                    break;
                case Checker.BOT_MOUNTAIN:
                    Toast.makeText(mContext, "Bottom Mountain clicked", Toast.LENGTH_SHORT).show();
                    field = Checker.BOT_MOUNTAIN;
                    break;
                case Checker.TOP_POOL:
                    Toast.makeText(mContext, "Top Pool clicked", Toast.LENGTH_SHORT).show();
                    field = Checker.TOP_POOL;
                    break;
                case Checker.TOP_MOUNTAIN:
                    Toast.makeText(mContext, "Top Mountain clicked", Toast.LENGTH_SHORT).show();
                    field = Checker.TOP_MOUNTAIN;
                    break;
                case Checker.CIRCLE:
                    Toast.makeText(mContext, "Circle clicked", Toast.LENGTH_SHORT).show();
                    field = Checker.CIRCLE;
                    if (isFourPerson()) {
                        dialogs.showTapDialogFourPlayers(mContext);
                    } else {
                        dialogs.showTapDialogThreePlayers(mContext);
                    }
                    break;
            }
            return false;
        }

        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
        @Override
        public boolean onDoubleTap(MotionEvent e) {
            final android.support.v7.app.ActionBar actionBar = pupsenActivity.getSupportActionBar();

            View decorView = pupsenActivity.getWindow().getDecorView();

            assert actionBar != null;
            if (actionBar.isShowing()) {
                decorView.setSystemUiVisibility(
                        View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                                | View.SYSTEM_UI_FLAG_FULLSCREEN);
                actionBar.hide();
                pupsenActivity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
            } else {
                decorView.setSystemUiVisibility(
                        View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
                actionBar.show();
                pupsenActivity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        pupsenActivity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
                        actionBar.hide();
                    }
                }, 4500);

            }
            return false;
        }
    }
}