package org.grrinc.preferencepool;

import android.util.Log;

/**
 * Created by guguza on 18.06.2015.
 */
public class Player {
    public static final String TAG = "PLAYER";
    private int id;
    private String name;
    private Status status;
    private Pool pool;
    private Mountain mountain;
    private Whist[] whists;
    private int numberOfTricks;

    public Player(int id, String name, int numberOfPlayers) {
        setId(id);
        setName(name);
        init(numberOfPlayers);
    }

    private void init(int numberOfPlayers) {
        initWhists(numberOfPlayers);
        pool = new Pool(id, 0);
        mountain = new Mountain(id, 0);
    }

    private void initWhists(int numberOfPlayers) {
        whists = new Whist[numberOfPlayers - 1];
        int n = 0;
        for(int i = 0; i < numberOfPlayers; i++) {
            if(id == i) {
                continue;
            }
            whists[n++] = new Whist(i);
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Pool getPool() {
        return pool;
    }

    public void setPool(Pool pool) {
        this.pool = pool;
    }

    public Mountain getMountain() {
        return mountain;
    }

    public void setMountain(Mountain mountain) {
        this.mountain = mountain;
    }

    public Whist[] getWhists() {
        return whists;
    }

    public void setWhists(Whist[] whists) {
        this.whists = whists;
    }

    public Whist getWhist(int id) {
        for (Whist item : whists) {
            if (item.getId() == id) {
                return item;
            }
        }
        return null;
    }

    //also must print whists array
    public void print() {
        Log.d(TAG, "ID: " + id + ", name is " + name + ", status = " + status +
                ", pool = " + pool.print() + ", mountain = " + mountain.print() +
                 ", tricks = " + numberOfTricks);
    }

    public int getNumberOfTricks() {
        return numberOfTricks;
    }

    public void setNumberOfTricks(int numberOfTricks) {
        this.numberOfTricks = numberOfTricks;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public enum Status {
        DEALER, WHIST, PASS, PLAYER };
}
