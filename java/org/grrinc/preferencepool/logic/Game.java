package org.grrinc.preferencepool.logic;

import org.grrinc.preferencepool.DealType;
import org.grrinc.preferencepool.Player;

/**
 * Created by guguza on 19.06.2015.
 */
public class Game {
    private Strategy strategy;

    public enum GameType { SOCHI, PETER, ROSTOV, CUSTOM }

    public Game(Strategy strategy){
        this.strategy = strategy;
    }

    public boolean executeStrategy(DealType dealType, Player[] players, int playerId, int numberOfWhisters) {
        return strategy.doOperation(dealType, players, playerId, numberOfWhisters);
    }
}
