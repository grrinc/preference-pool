package org.grrinc.preferencepool.logic;

import org.grrinc.preferencepool.DealType;
import org.grrinc.preferencepool.Player;

/**
 * Created by guguza on 19.06.2015.
 */
public class RostovGame extends Strategy {

    public static final int SIX_VALUE     = 2;
    public static final int SEVEN_VALUE   = 4;
    public static final int EIGHT_VALUE   = 6;
    public static final int ALLPASS_VALUE = 0;
    public static final int NINE_VALUE    = 8;
    public static final int TEN_VALUE     = 10;
    public static final int MISERE_VALUE  = 10;

    private void initMultiplier() {
        DealType.SIX.setMultiplierPool(SIX_VALUE);
        DealType.SEVEN.setMultiplierPool(SEVEN_VALUE);
        DealType.EIGHT.setMultiplierPool(EIGHT_VALUE);
        DealType.ALLPASS.setMultiplierPool(ALLPASS_VALUE);
        DealType.NINE.setMultiplierPool(NINE_VALUE);
        DealType.TEN.setMultiplierPool(TEN_VALUE);
        DealType.MISERE.setMultiplierPool(MISERE_VALUE);
    }

    @Override
    public boolean endGame(Player[] players) {
        return false;
    }
}
