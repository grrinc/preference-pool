package org.grrinc.preferencepool.logic;

import org.grrinc.preferencepool.DealType;
import org.grrinc.preferencepool.Player;
import org.grrinc.preferencepool.Whist;

/**
 * Created by guguza on 19.06.2015.
 */

public abstract class Strategy {
    public int whistType = Whist.GENTLEMANLY;// or Whist.GENTLEMANLY, depends on settings

    public abstract boolean endGame(Player[] players);

    public boolean doOperation(DealType dealType, Player[] players, int playerId, int numberOfWhisters) {
        switch (dealType) {
            case SIX:
                calculate(dealType, players, playerId, numberOfWhisters, 6, 4);
                break;
            case SEVEN:
                calculate(dealType, players, playerId, numberOfWhisters, 7, 2);
                break;
            case EIGHT:
                calculate(dealType, players, playerId, numberOfWhisters, 8, 1);
                break;
            case ALLPASS:
                //new algorithm
                /*если распасы увеличиваются, то штраф(youGotBad) за каждую взятку увеличивается*/
                int youGotBad = 1;
                //каждому записывается
                for(Player player: players) {
                    player.getMountain().addQuantity(youGotBad * player.getNumberOfTricks());
                }
                break;
            case NINE:
                calculate(dealType, players, playerId, numberOfWhisters, 9, 1);
                break;
            case TEN:
                //maybe function reloading?
                calculate(dealType, players, playerId, numberOfWhisters, 10, 0);
                break;
            case MISERE:
                if(players[playerId].getNumberOfTricks() != 0) {
                    players[playerId].getMountain().addQuantity(players[playerId].
                            getNumberOfTricks() * DealType.MISERE.getMultiplierPool());
                } else {
                    players[playerId].getPool().addQuantity(DealType.MISERE.getMultiplierPool());
                }
                //new algorithm
                break;
        }
        return false;
    }

    private void calculate(DealType dealType, Player[] players, int playerId, int numberOfWhisters
            ,int playerBinding, int whisterBinding) {
        int multiplier = dealType.getMultiplierPool();
        //если игра успешна, пишем пулю
        if (players[playerId].getNumberOfTricks() >= playerBinding) {
            players[playerId].getPool().addQuantity(multiplier);
            //иначе в гору
        } else {
            players[playerId].getMountain().addQuantity(multiplier * (playerBinding
                    - players[playerId].getNumberOfTricks()));
        }
        //если вистующих нет
        if (numberOfWhisters == 0) {
            players[playerId].getPool().addQuantity(multiplier);
        }
        //если один вистующий
        if (numberOfWhisters == 1) {
            //ищем его
            for (Player whister : players) {
                if (whister.getStatus() == Player.Status.WHIST) {
                    //если вист джентельменский
                    if(whistType == Whist.GREEDY) {
                        //вистующий наслаждается вистами
                        whister.getWhist(playerId).addQuantity((10 - players[playerId]
                                .getNumberOfTricks()) * multiplier);
                    } else if(whistType == Whist.GENTLEMANLY) {
                        //записываем половину вистов вистующему
                        whister.getWhist(playerId).addQuantity(((10 - players[playerId]
                                .getNumberOfTricks()) * multiplier) / 2);
                        //ищем пасующего
                        for (Player passer : players) {
                            if (passer.getStatus() == Player.Status.PASS) {
                                ////записываем половину вистов пасующему
                                passer.getWhist(playerId).addQuantity(((10 - players[playerId]
                                        .getNumberOfTricks()) * multiplier) / 2);
                            }
                        }
                    }
                    //если недобор, пишем еще в гору
                    if (whisterBinding > whister.getNumberOfTricks()) {
                        whister.getMountain().addQuantity((whisterBinding - whister
                                .getNumberOfTricks()) * multiplier);
                    }
                    break;
                }
            }
            //если два вистующих)
        } else if (numberOfWhisters == 2) {
            int tricks = 10 - players[playerId].getNumberOfTricks();
            if(whistType == Whist.GENTLEMANLY) {
                //ищем каждого и
                for (Player whister : players) {
                    if (whister.getStatus() == Player.Status.WHIST) {
                        //добавляем висты пополам
                        whister.getWhist(playerId).addQuantity((tricks * multiplier) / 2);
                        //при недоборе в гору пишем пополам
                        if (whisterBinding > tricks) {
                            whister.getMountain().addQuantity(((whisterBinding - tricks) / 2) * multiplier);
                        }
                    }
                }
            } else if(whistType == Whist.GREEDY) {
                //ищем каждого и
                for (Player whister : players) {
                    if (whister.getStatus() == Player.Status.WHIST) {
                        //добавляем висты
                        whister.getWhist(playerId).addQuantity(whister.getNumberOfTricks() * multiplier);
                        //при недоборе в гору пишем тому, кто лох и больше недобрал
                        if (whisterBinding > tricks) {
                            if(whister.getNumberOfTricks() < tricks - whister.getNumberOfTricks())
                            whister.getMountain().addQuantity((whisterBinding - tricks) * multiplier);
                        }
                    }
                }
            }

        }

    }
}