package org.grrinc.preferencepool;

import android.util.Log;

/**
 * Created by guguza on 18.06.2015.
 */
public class Whist extends Score {
    public static final String TAG = "WHIST";
    public static final int GREEDY      = 0;
    public static final int GENTLEMANLY = 1;

    private static float price;

    public Whist(int id, int quantity) {
        super(id, quantity);
    }

    public Whist(int id) {
        super(id);
    }

    public static void setPrice(float value) {
        price = value;
    }

    public static float getPrice() {
        return price;
    }

    public float calculate() {
        return getQuantity() * price;
    }

    @Override
    public String print() {
        String info = "ID: " + getId() + ", quantity = " + getQuantity() + ", price = " + price;
        Log.d(TAG, info);
        return info;
    }

}
